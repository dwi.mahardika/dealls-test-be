package hash

import (
	"golang.org/x/crypto/bcrypt"
)

type Hasher interface {
	Hash(password []byte) []byte
	Verify(hashed, password string) bool
}

type bcryptHash struct {
	cost int
}

func NewBcryptHash(cost int) Hasher {
	return &bcryptHash{cost: cost}
}

func (bc *bcryptHash) Hash(password []byte) []byte {
	hash, err := bcrypt.GenerateFromPassword(password, bc.cost)
	if err == nil {
		return hash
	}

	return nil
}

func (bc *bcryptHash) Verify(hashed, password string) bool {
	err := bcrypt.CompareHashAndPassword([]byte(hashed), []byte(password))
	return err == nil
}
