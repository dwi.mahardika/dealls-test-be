package jwt

import (
	"errors"
	"time"

	"github.com/dgrijalva/jwt-go"
	conf "gitlab.com/rockondev/go-commons/config"
	"gitlab.com/rockondev/go-commons/logger"
)

var jwtKey = []byte(conf.Get("JWT_KEY", ""))

type Claims struct {
	Username string `json:"username"`
	jwt.StandardClaims
}

type JWT interface {
	GenerateJWT(username string) (string, error)
	ValidateJWT(tokenString string) (*Claims, error)
}

type jwtService struct{}

func NewJWT() JWT {
	return &jwtService{}
}

func (j *jwtService) GenerateJWT(username string) (string, error) {
	// Set up token claims
	expirationTime := time.Now().Add(24 * time.Hour)
	claims := &Claims{
		Username: username,
		StandardClaims: jwt.StandardClaims{
			ExpiresAt: expirationTime.Unix(),
		},
	}

	// Create the token using the claims
	token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)

	// Sign the token with our secret key
	return token.SignedString(jwtKey)
}

func (j *jwtService) ValidateJWT(tokenString string) (*Claims, error) {
	claims := &Claims{}
	token, err := jwt.ParseWithClaims(tokenString, claims, func(token *jwt.Token) (interface{}, error) {
		return jwtKey, nil
	})

	if err != nil {
		logger.Error("Failed to parse token: ", err.Error())
		return nil, err
	}

	if !token.Valid {
		logger.Error("Invalid token")
		return nil, errors.New("token not valid")
	}

	return claims, nil
}
