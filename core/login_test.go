package core

import (
	"testing"

	"gitlab.com/dwi.mahardika/dealls-test-be/external/hash"
	"gitlab.com/dwi.mahardika/dealls-test-be/external/jwt"
	"gitlab.com/dwi.mahardika/dealls-test-be/repositories"
)

func Test_login_Login(t *testing.T) {
	type fields struct {
		userRepo repositories.UserRepository
		hasher   hash.Hasher
		jwt      jwt.JWT
	}
	type args struct {
		userReq  string
		password string
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    string
		wantErr bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			l := &login{
				userRepo: tt.fields.userRepo,
				hasher:   tt.fields.hasher,
				jwt:      tt.fields.jwt,
			}
			got, err := l.Login(tt.args.userReq, tt.args.password)
			if (err != nil) != tt.wantErr {
				t.Errorf("login.Login() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if got != tt.want {
				t.Errorf("login.Login() = %v, want %v", got, tt.want)
			}
		})
	}
}
