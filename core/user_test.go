package core

import (
	"context"
	"testing"

	"gitlab.com/dwi.mahardika/dealls-test-be/external/hash"
	"gitlab.com/dwi.mahardika/dealls-test-be/models"
	"gitlab.com/dwi.mahardika/dealls-test-be/repositories"
)

func Test_user_SignUp(t *testing.T) {
	type fields struct {
		userRepo repositories.UserRepository
		hasher   hash.Hasher
	}
	type args struct {
		ctx  context.Context
		user *models.User
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		wantErr bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			u := &user{
				userRepo: tt.fields.userRepo,
				hasher:   tt.fields.hasher,
			}
			if err := u.SignUp(tt.args.ctx, tt.args.user); (err != nil) != tt.wantErr {
				t.Errorf("user.SignUp() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}
