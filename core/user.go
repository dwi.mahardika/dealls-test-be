package core

import (
	"context"
	"errors"
	"strings"
	"time"

	"gitlab.com/dwi.mahardika/dealls-test-be/external/hash"
	"gitlab.com/dwi.mahardika/dealls-test-be/models"
	"gitlab.com/dwi.mahardika/dealls-test-be/repositories"
	"gitlab.com/rockondev/go-commons/logger"
)

type User interface {
	SignUp(ctx context.Context, user *models.User) error
	ListAllUser(ctx context.Context) ([]models.User, error)
	GetUserData(ctx context.Context) (*models.User, error)
	Swipe(ctx context.Context, targetUsername string, direction string) error
	SetPremium(ctx context.Context) error
}

type user struct {
	userRepo repositories.UserRepository
	hasher   hash.Hasher
}

func NewUser(userRepo repositories.UserRepository, hash hash.Hasher) User {
	return &user{userRepo: userRepo, hasher: hash}
}

func (u *user) SignUp(ctx context.Context, user *models.User) error {
	auth := ctx.Value("Authotization")
	logger.Info(auth)
	user.Username = trimString(user.Username)
	user.Email = trimString(user.Email)
	user.Sex = trimString(user.Sex)
	hashPwd := u.hasher.Hash([]byte(user.Password))
	user.Password = string(hashPwd)
	err := u.userRepo.CreateUser(user)
	if err != nil {
		logger.Error("failed create user, err: ", err.Error())
		return err
	}
	return nil
}

func (u *user) ListAllUser(ctx context.Context) ([]models.User, error) {
	username, ok := ctx.Value("username").(string)
	if !ok {
		logger.Error("username not found in context")
		return nil, errors.New("username not found in context")
	}

	currentUser, err := u.userRepo.GetUserByUsernameOrEmail(username)
	if err != nil {
		logger.Error("failed to get current user, err: ", err.Error())
		return nil, err
	}

	var users []models.User
	users, err = u.userRepo.GetAllUsersExcludingSwiped(currentUser.ID)
	if err != nil {
		logger.Error("failed to get users, err: ", err.Error())
		return nil, err
	}

	for _, x := range users {
		x.Verified = x.Premium
	}

	return users, nil
}

func (u *user) GetUserData(ctx context.Context) (*models.User, error) {
	username, ok := ctx.Value("username").(string)
	if !ok {
		logger.Error("username not found in context")
		return nil, errors.New("username not found in context")
	}

	currentUser, err := u.userRepo.GetUserByUsernameOrEmail(username)
	if err != nil {
		logger.Error("failed to get current user, err: ", err.Error())
		return nil, err
	}

	currentUser.Verified = currentUser.Premium

	return currentUser, nil
}

func (u *user) Swipe(ctx context.Context, targetUsername string, direction string) error {
	username, ok := ctx.Value("username").(string)
	if !ok {
		logger.Error("username not found in context")
		return errors.New("username not found in context")
	}

	currentUser, err := u.userRepo.GetUserByUsernameOrEmail(username)
	if err != nil {
		logger.Error("failed to get current user, err: ", err.Error())
		return err
	}

	targetUser, err := u.userRepo.GetUserByUsernameOrEmail(targetUsername)
	if err != nil {
		logger.Error("failed to get target user, err: ", err.Error())
		return err
	}

	if currentUser.Premium {
		err = u.userRepo.RecordSwipe(currentUser.ID, targetUser.ID, direction)
		return err
	}

	// Get today's date in YYYY-MM-DD format
	today := time.Now().Format("2006-01-02")

	// Count swipes for today
	count, err := u.userRepo.CountUserSwipes(currentUser.ID, today)
	if err != nil {
		logger.Error("failed to count user swipes, err: ", err.Error())
		return err
	}

	if count >= 5 {
		return errors.New("daily swipe limit reached")
	}

	// Record the swipe
	err = u.userRepo.RecordSwipe(currentUser.ID, targetUser.ID, direction)
	if err != nil {
		logger.Error("failed to record swipe, err: ", err.Error())
		return err
	}

	return nil
}

func (u *user) SetPremium(ctx context.Context) error {
	username, ok := ctx.Value("username").(string)
	if !ok {
		logger.Error("username not found in context")
		return errors.New("username not found in context")
	}

	currentUser, err := u.userRepo.GetUserByUsernameOrEmail(username)
	if err != nil {
		logger.Error("failed to get current user, err: ", err.Error())
		return err
	}

	if currentUser.Premium {
		logger.Info("already premium user no need to change")
		return errors.New("already premium user no need to change")
	}

	err = u.userRepo.UpdatePremium(currentUser.Username)
	if err != nil {
		logger.Error("failed update to premium")
		return err
	}

	return nil
}

func trimString(s string) string {
	s = strings.TrimSpace(s)
	s = strings.ToLower(s)
	return s
}
