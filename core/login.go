package core

import (

	// "github.com/dgrijalva/jwt-go"
	"errors"

	"gitlab.com/dwi.mahardika/dealls-test-be/external/hash"
	"gitlab.com/dwi.mahardika/dealls-test-be/external/jwt"
	"gitlab.com/dwi.mahardika/dealls-test-be/repositories"
	"gitlab.com/rockondev/go-commons/logger"
)

type Login interface {
	Login(username, password string) (string, error)
}

type login struct {
	userRepo repositories.UserRepository
	hasher   hash.Hasher
	jwt      jwt.JWT
}

func NewLogin(userRepo repositories.UserRepository, hash hash.Hasher, jwt jwt.JWT) Login {
	return &login{userRepo: userRepo, hasher: hash, jwt: jwt}
}

func (l *login) Login(userReq, password string) (string, error) {

	// find user and get hashPassword
	user, err := l.userRepo.GetUserByUsernameOrEmail(userReq)
	if err != nil {
		logger.Error("user not exists, err: ", err.Error())
		return "", errors.New("user not exists")
	}

	//compare pwd and password from table
	ok := l.hasher.Verify(user.Password, password)
	if !ok {
		logger.Error("password not match")
		return "", errors.New("password not match")
	}

	// generate jwt
	token, err := l.jwt.GenerateJWT(user.Username)
	if err != nil {
		logger.Error("failed generate jwt")
		return "", err
	}

	return token, nil
}
