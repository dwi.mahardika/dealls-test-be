
## Menjalankan aplikasi
Berikut langkah-langkah menggunakan aplikasi ini:
1. buatkan database dengan nama ```dealls-test-dating``` (bisa di sesuaikan dengan nama DB_NAME pada .env) pada postgresql (karena saya membuatnya untuk menggunakan postgresql)
2. copy file .env.example ke file .env contoh pada terminal: ```cp .env.example .env```
3. sesuaikan variable pada file .env dengan kebutuhan anda
4. lalu jalankan aplikasi dengan menggunakan syntax ```go run main.go``` pada terminal

Jika anda menggunakan docker, bisa anda build imagenya dengan perintah docker
```build -t dating-app-test:latest``` dan sebelum run pastikan environtment di isi semua karena bersifat mandatory, contohnya ada pada .env.example

contoh run aplikasi

```docker run -p 8080:8080 -e DB_HOST='localhost' -e DB_USER='postgres' -e DB_PASSWORD='postgres' -e DB_PORT='5432' -e DB_NAME='xyz-finance' -e DB_OPTIONS='?sslmode=disable' -e MAX_SWIPE_QUOTA='10' dating-app-test:latest```

## Fitur aplikasi	
1. Login dan autentikasi menggunakan JWT
2. Premium user unlimited swipe dan free user hanya bisa 10 swipe.

## Cara Kerja aplikasi ini
Swagger saya siapkan [disini](https://gitlab.com/dwi.mahardika/dealls-test-be/DeallsDatingAppTest.swagger.yaml "Swagger")        
Langkah-langkahnya :  
1. Login user, endpoint `/login` disini saya siapkan dummy user dengan username `jonoganteng1` dan password `user` .
2. Jika ingin membuat user baru, anda bisa Sign Up di endpoint `/signup` terlebih dahulu baru login.
3. Pada endpoint `/home` anda harus mengirimkan token dari login pada Authorization Bearer token atau pada header tambahkan key `Authorization` dengan value `Bearer ` + token tadi. 
Pada endpoint ini akan muncul object user yang belum user swipe.
4. Pada endpoint `/swipe` ini digunakan ketika user trigger swipe, data user yang swipe dan target yang di swipe akan disimpan dalam database, dan jika user premium akan memiliki unlimited swipe, jika user biasa hanya bisa 10 swipe per harinya
5. Pada endpoint `/profile` hanya menampilkan profile saat ini.
6. Pada endpoint `/premium` , free user bisa mengupdate dirinya agar menjadi premium user

Semua endpoint kecuali login memiliki autentikasi token, jika belum login atau tidak mengisi token pada Authorization, maka akan meresponse dengan invalid token.


## Architecture
Aplikasi ini sudah menerapkan clean architecture, dimana syarat clean architecture adalah :  
1. Presentation Layer: Berisi HTTP handlers yang menerima dan memproses request dari pengguna. (Handler terpisah dengan logic, dan templates juga termasuk ini)
2. Application Layer: Berisi use cases yang merupakan logika bisnis utama. Ini mengacu pada folder core/ yang hanya berisi logic.
3. Domain Layer: Berisi entity dan repository interface. (model dan repository terpisah)
4. Infrastructure Layer: Berisi implementasi repository, konfigurasi database, dan utilitas lainnya. (handler, migrations db, config db, gcs, redis, kms terpisah - > belum sempat dibuat, hehe)

  
## Note
Mohon maaf jika telat karena merubah deadline, tadinya mau dibuat sesimple mungkin tanpa ada db, migration script, hanya array user biasa tapi saya mau memaksimalkan kinerja saya disini. Dan maaf jika FE atau tampilan tidak include karena waktu tidak cukup.