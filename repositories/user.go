package repositories

import (
	"time"

	"gitlab.com/dwi.mahardika/dealls-test-be/models"
	"gorm.io/gorm"
)

type userRepository struct {
	db *gorm.DB
}

type UserRepository interface {
	CreateUser(user *models.User) error
	GetUserByUsernameOrEmail(usernameOrEmail string) (*models.User, error)
	GetAllUsersExcludingSwiped(userID uint) ([]models.User, error)
	CountUserSwipes(userID uint, date string) (int64, error)
	RecordSwipe(userID uint, targetUserID uint, direction string) error
	GetSwipedUsers(userID uint) ([]models.User, error)
	UpdatePremium(username string) error
}

func NewUserRepository(db *gorm.DB) UserRepository {
	return &userRepository{db: db}
}

func (r *userRepository) CreateUser(user *models.User) error {
	return r.db.Create(user).Error
}

func (r *userRepository) GetUserByUsernameOrEmail(usernameOrEmail string) (*models.User, error) {
	var user models.User
	if err := r.db.Where("username = ? OR email = ?", usernameOrEmail, usernameOrEmail).First(&user).Error; err != nil {
		return nil, err
	}
	return &user, nil
}

func (r *userRepository) GetAllUsersExcludingSwiped(userID uint) ([]models.User, error) {
	var users []models.User
	err := r.db.Raw(`
		SELECT * FROM mst_user 
		WHERE id NOT IN (
			SELECT target_user_id FROM swipes WHERE user_id = ?
		)`, userID).Scan(&users).Error
	return users, err
}

func (r *userRepository) GetUsersByGenderExcludingSwiped(userID uint, gender string) ([]models.User, error) {
	var users []models.User
	err := r.db.Raw(`
		SELECT * FROM mst_user 
		WHERE sex = ? AND id NOT IN (
			SELECT target_user_id FROM swipes WHERE user_id = ?
		)`, gender, userID).Scan(&users).Error
	return users, err
}

func (r *userRepository) CountUserSwipes(userID uint, date string) (int64, error) {
	var count int64
	err := r.db.Model(&models.Swipe{}).Where("user_id = ? AND DATE(swipe_date) = ?", userID, date).Count(&count).Error
	return count, err
}

func (r *userRepository) RecordSwipe(userID uint, targetUserID uint, direction string) error {
	swipe := &models.Swipe{
		UserID:       userID,
		TargetUserID: targetUserID,
		Direction:    direction,
		SwipeDate:    time.Now(),
	}
	return r.db.Create(swipe).Error
}

func (r *userRepository) GetSwipedUsers(userID uint) ([]models.User, error) {
	var swipedUsers []models.User
	err := r.db.Raw(`
		SELECT * FROM mst_user 
		WHERE id IN (
			SELECT target_user_id FROM swipes WHERE user_id = ?
		)`, userID).Scan(&swipedUsers).Error
	return swipedUsers, err
}

func (r *userRepository) UpdatePremium(username string) error {
	return r.db.Model(&models.User{}).Where("username = ?", username).Updates(map[string]interface{}{
		"premium": true,
	}).Error
}
