-- +goose Up
-- +goose StatementBegin

-- insert dummy data
-- $2a$10$KbY6EtYPd2rb2E9jvmJc2.r.XFiMonBOzOAjn0VBbQfcOWPvmo/A. == user
INSERT INTO mst_user(fullname, username, email, password, sex, premium) VALUES 
    ('Jane1', 'janebeauty1', 'jane.beauty1@example.com', '$2a$10$KbY6EtYPd2rb2E9jvmJc2.r.XFiMonBOzOAjn0VBbQfcOWPvmo/A.', 'female', true),
    ('Jane2', 'janebeauty2', 'jane.beauty2@example.com', '$2a$10$KbY6EtYPd2rb2E9jvmJc2.r.XFiMonBOzOAjn0VBbQfcOWPvmo/A.', 'female', false),
    ('Jane3', 'janebeauty3', 'jane.beauty3@example.com', '$2a$10$KbY6EtYPd2rb2E9jvmJc2.r.XFiMonBOzOAjn0VBbQfcOWPvmo/A.', 'female', false),
    ('Jane4', 'janebeauty4', 'jane.beauty4@example.com', '$2a$10$KbY6EtYPd2rb2E9jvmJc2.r.XFiMonBOzOAjn0VBbQfcOWPvmo/A.', 'female', false),
    ('Jane5', 'janebeauty5', 'jane.beauty5@example.com', '$2a$10$KbY6EtYPd2rb2E9jvmJc2.r.XFiMonBOzOAjn0VBbQfcOWPvmo/A.', 'female', false),
    ('Jane6', 'janebeauty6', 'jane.beauty6@example.com', '$2a$10$KbY6EtYPd2rb2E9jvmJc2.r.XFiMonBOzOAjn0VBbQfcOWPvmo/A.', 'female', false),
    ('Jane7', 'janebeauty7', 'jane.beauty7@example.com', '$2a$10$KbY6EtYPd2rb2E9jvmJc2.r.XFiMonBOzOAjn0VBbQfcOWPvmo/A.', 'female', false),
    ('Jane8', 'janebeauty8', 'jane.beauty8@example.com', '$2a$10$KbY6EtYPd2rb2E9jvmJc2.r.XFiMonBOzOAjn0VBbQfcOWPvmo/A.', 'female', false),
    ('Jane9', 'janebeauty9', 'jane.beauty9@example.com', '$2a$10$KbY6EtYPd2rb2E9jvmJc2.r.XFiMonBOzOAjn0VBbQfcOWPvmo/A.', 'female', false),
    ('Jane10', 'janebeauty10', 'jane.beauty10@example.com', '$2a$10$KbY6EtYPd2rb2E9jvmJc2.r.XFiMonBOzOAjn0VBbQfcOWPvmo/A.', 'female', false),
    ('Jane11', 'janebeauty11', 'jane.beauty11@example.com', '$2a$10$KbY6EtYPd2rb2E9jvmJc2.r.XFiMonBOzOAjn0VBbQfcOWPvmo/A.', 'female', false),
    ('Jane12', 'janebeauty12', 'jane.beauty12@example.com', '$2a$10$KbY6EtYPd2rb2E9jvmJc2.r.XFiMonBOzOAjn0VBbQfcOWPvmo/A.', 'female', false),
    ('Jane13', 'janebeauty13', 'jane.beauty13@example.com', '$2a$10$KbY6EtYPd2rb2E9jvmJc2.r.XFiMonBOzOAjn0VBbQfcOWPvmo/A.', 'female', false),
    ('Jane14', 'janebeauty14', 'jane.beauty14@example.com', '$2a$10$KbY6EtYPd2rb2E9jvmJc2.r.XFiMonBOzOAjn0VBbQfcOWPvmo/A.', 'female', false),
    ('Jane15', 'janebeauty15', 'jane.beauty15@example.com', '$2a$10$KbY6EtYPd2rb2E9jvmJc2.r.XFiMonBOzOAjn0VBbQfcOWPvmo/A.', 'female', false),
    ('Jono1', 'jonoganteng1', 'jono.ganteng1@example.com', '$2a$10$KbY6EtYPd2rb2E9jvmJc2.r.XFiMonBOzOAjn0VBbQfcOWPvmo/A.', 'male', true),
    ('Jono2', 'jonoganteng2', 'jono.ganteng2@example.com', '$2a$10$KbY6EtYPd2rb2E9jvmJc2.r.XFiMonBOzOAjn0VBbQfcOWPvmo/A.', 'male', false),
    ('Jono3', 'jonoganteng3', 'jono.ganteng3@example.com', '$2a$10$KbY6EtYPd2rb2E9jvmJc2.r.XFiMonBOzOAjn0VBbQfcOWPvmo/A.', 'male', false),
    ('Jono4', 'jonoganteng4', 'jono.ganteng4@example.com', '$2a$10$KbY6EtYPd2rb2E9jvmJc2.r.XFiMonBOzOAjn0VBbQfcOWPvmo/A.', 'male', false),
    ('Jono5', 'jonoganteng5', 'jono.ganteng5@example.com', '$2a$10$KbY6EtYPd2rb2E9jvmJc2.r.XFiMonBOzOAjn0VBbQfcOWPvmo/A.', 'male', false);
-- +goose StatementEnd

-- +goose Down
-- +goose StatementBegin
TRUNCATE TABLE mst_user;
-- +goose StatementEnd
