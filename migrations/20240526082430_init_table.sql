-- +goose Up
-- +goose StatementBegin
CREATE TABLE IF NOT EXISTS mst_user (
    fullname VARCHAR(100),
    username VARCHAR(100) PRIMARY KEY,
    email VARCHAR(150),
    password VARCHAR(100),
    sex VARCHAR(10) NULL,
    premium bool,
    created_at TIMESTAMPTZ NULL DEFAULT current_timestamp,
    updated_at TIMESTAMPTZ NULL DEFAULT current_timestamp,
    deleted_at TIMESTAMPTZ NULL DEFAULT NULL
);

-- +goose StatementEnd

-- +goose Down
-- +goose StatementBegin
DROP TABLE IF EXISTS mst_user;
-- +goose StatementEnd
