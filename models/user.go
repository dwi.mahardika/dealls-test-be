package models

import "gorm.io/gorm"

type User struct {
	gorm.Model
	Fullname string `json:"fullname"`
	Username string `gorm:"uniqueIndex" json:"username"`
	Password string `json:"password"`
	Email    string `gorm:"uniqueIndex" json:"email"`
	Premium  bool   `gorm:"default:0" json:"premium"`
	Sex      string `json:"sex"`
	Verified bool   `gorm:"-" json:"verified"`
}

func (User) TableName() string {
	return "mst_user"
}
