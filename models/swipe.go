package models

import (
	"time"

	"gorm.io/gorm"
)

type Swipe struct {
	gorm.Model
	UserID       uint
	TargetUserID uint
	Direction    string // "left" or "right"
	SwipeDate    time.Time
}
