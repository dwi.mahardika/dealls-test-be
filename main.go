package main

import (
	"database/sql"
	"fmt"
	"log"

	"github.com/gin-gonic/gin"
	"github.com/joho/godotenv"
	"github.com/pressly/goose"
	"gitlab.com/dwi.mahardika/dealls-test-be/core"
	"gitlab.com/dwi.mahardika/dealls-test-be/external/hash"
	"gitlab.com/dwi.mahardika/dealls-test-be/external/jwt"
	"gitlab.com/dwi.mahardika/dealls-test-be/handlers"
	"gitlab.com/dwi.mahardika/dealls-test-be/models"
	"gitlab.com/dwi.mahardika/dealls-test-be/repositories"
	"gitlab.com/rockondev/go-commons/config"
	"gitlab.com/rockondev/go-commons/logger"
	"gorm.io/driver/postgres"
	"gorm.io/gorm"
	gormLog "gorm.io/gorm/logger"
)

func main() {
	godotenv.Load()

	DB_USER := config.Get("DB_USER", "")
	DB_PASSWORD := config.Get("DB_PASSWORD", "")
	DB_HOST := config.Get("DB_HOST", "")
	DB_PORT := config.Get("DB_PORT", "")
	DB_NAME := config.Get("DB_NAME", "")
	DB_OPTIONS := config.Get("DB_OPTIONS", "")

	gormConfig := &gorm.Config{
		Logger: gormLog.Default.LogMode(gormLog.Info),
	}

	dsn := fmt.Sprintf("postgres://%s:%s@%s:%s/%s%s", DB_USER, DB_PASSWORD, DB_HOST, DB_PORT, DB_NAME, DB_OPTIONS)
	db, err := gorm.Open(postgres.Open(dsn), gormConfig)
	if err != nil {
		logger.Error("Failed to connect to database, err: ", err.Error())
		return
	}
	err = db.AutoMigrate(models.User{}, models.Swipe{})
	if err != nil {
		logger.Error("failed create table with gorm, err: ", err.Error())
	}

	//migration data with goose
	sqlDB, err := db.DB()
	if err != nil {
		logger.Error("failed get sqlDB from gorm, err: ", err.Error())
	}
	runMigrations(sqlDB)

	userRepo := repositories.NewUserRepository(db)

	// I set default bycrpt cost = 10
	bycrptCost := 10
	hasher := hash.NewBcryptHash(bycrptCost)
	jwtService := jwt.NewJWT()

	loginCore := core.NewLogin(userRepo, hasher, jwtService)
	userCore := core.NewUser(userRepo, hasher)

	authMiddleware := handlers.NewAuthMiddleware(jwtService)

	r := gin.Default()

	handlers.RegisterHandler(r,
		handlers.NewLoginHandler(loginCore),
		handlers.NewUserHandler(userCore, authMiddleware),
	)

	log.Println("Server started at :8080")
	if err := r.Run(":8080"); err != nil {
		log.Fatalf("Failed to start server: %v", err)
	}
}

func runMigrations(db *sql.DB) {
	// Set directory for migrations
	if err := goose.SetDialect("postgres"); err != nil {
		log.Fatalf("goose.SetDialect: %v", err)
	}

	migrationsDir := "./migrations"
	if err := goose.Up(db, migrationsDir); err != nil {
		log.Fatalf("goose.Up: %v", err)
	}
}
