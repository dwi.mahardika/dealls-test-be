package handlers

import (
	"net/http"

	"github.com/gin-gonic/gin"
	"gitlab.com/dwi.mahardika/dealls-test-be/core"
	"gitlab.com/dwi.mahardika/dealls-test-be/models"
	"gitlab.com/rockondev/go-commons/logger"
)

type userHandler struct {
	userCore       core.User
	authMiddleware *AuthMiddleware
}

func NewUserHandler(user core.User, authMd *AuthMiddleware) Handler {
	return &userHandler{userCore: user, authMiddleware: authMd}
}

func (u *userHandler) InitializeRoute(r *gin.Engine) {
	r.POST("/signup", u.signUp)
	r.GET("/home", u.authMiddleware.Authorize(), u.home)
	r.GET("/", u.authMiddleware.Authorize(), u.home)
	r.GET("/profile", u.authMiddleware.Authorize(), u.profile)
	r.POST("/swipe", u.authMiddleware.Authorize(), u.swipe)
	r.PUT("/premium", u.authMiddleware.Authorize(), u.setPremium)
}

func (u *userHandler) signUp(ctx *gin.Context) {

	var user models.User
	if err := ctx.ShouldBindJSON(&user); err != nil {
		ctx.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	err := u.userCore.SignUp(ctx, &user)
	if err != nil {
		logger.Error("failed sign up, err: ", err.Error())
		ctx.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
		return
	}

	ctx.JSON(http.StatusCreated, gin.H{"message": "user successfuly created"})

}

func (h *userHandler) home(ctx *gin.Context) {
	users, err := h.userCore.ListAllUser(ctx.Request.Context())
	if err != nil {
		logger.Error("failed to get users, err: ", err.Error())
		ctx.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
		return
	}

	ctx.JSON(http.StatusOK, users)
}

func (u *userHandler) swipe(ctx *gin.Context) {
	var request struct {
		TargetUsername string `json:"target_username"`
		Direction      string `json:"direction"` // "left" or "right"
	}

	if err := ctx.ShouldBindJSON(&request); err != nil {
		ctx.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	if request.Direction != "left" && request.Direction != "right" {
		ctx.JSON(http.StatusBadRequest, gin.H{"error": "invalid direction"})
		return
	}

	err := u.userCore.Swipe(ctx.Request.Context(), request.TargetUsername, request.Direction)
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
		return
	}

	ctx.JSON(http.StatusOK, gin.H{"status": "swipe recorded"})
}

func (u *userHandler) setPremium(ctx *gin.Context) {
	err := u.userCore.SetPremium(ctx.Request.Context())
	if err != nil {
		logger.Error("failed to update premium, err: ", err.Error())
		ctx.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
		return
	}

	ctx.JSON(http.StatusOK, gin.H{"message": "updated to premium!"})
}

func (h *userHandler) profile(ctx *gin.Context) {
	user, err := h.userCore.GetUserData(ctx.Request.Context())
	if err != nil {
		logger.Error("failed to get profile, err: ", err.Error())
		ctx.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
		return
	}

	ctx.JSON(http.StatusOK, user)
}
