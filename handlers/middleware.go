package handlers

import (
	"context"
	"net/http"
	"strings"

	"github.com/gin-gonic/gin"
	"gitlab.com/dwi.mahardika/dealls-test-be/external/jwt"
	"gitlab.com/rockondev/go-commons/logger"
)

type AuthMiddleware struct {
	jwt jwt.JWT
}

func NewAuthMiddleware(jwt jwt.JWT) *AuthMiddleware {
	return &AuthMiddleware{jwt: jwt}
}

func (m *AuthMiddleware) Authorize() gin.HandlerFunc {
	return func(ctx *gin.Context) {
		authHeader := ctx.GetHeader("Authorization")
		if authHeader == "" {
			ctx.JSON(http.StatusUnauthorized, gin.H{"error": "Authorization header required"})
			ctx.Abort()
			return
		}

		tokenString := strings.TrimPrefix(authHeader, "Bearer ")
		claims, err := m.jwt.ValidateJWT(tokenString)
		if err != nil {
			logger.Error("Invalid token, err: ", err.Error())
			ctx.JSON(http.StatusUnauthorized, gin.H{"error": "Invalid token"})
			ctx.Abort()
			return
		}

		newCtx := context.WithValue(ctx.Request.Context(), "username", claims.Username)
		ctx.Request = ctx.Request.WithContext(newCtx)
		ctx.Next()
	}
}
