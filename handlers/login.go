package handlers

import (
	"net/http"
	"strings"

	"github.com/gin-gonic/gin"
	"gitlab.com/dwi.mahardika/dealls-test-be/core"
	"gitlab.com/dwi.mahardika/dealls-test-be/models"
	"gitlab.com/rockondev/go-commons/logger"
)

type loginHandler struct {
	loginCore core.Login
}

func NewLoginHandler(login core.Login) Handler {
	return &loginHandler{loginCore: login}
}

func (l *loginHandler) InitializeRoute(r *gin.Engine) {
	r.POST("/login", l.login)
}

func (l *loginHandler) login(ctx *gin.Context) {
	var user models.User
	if err := ctx.ShouldBindJSON(&user); err != nil {
		ctx.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	user.Username = strings.TrimSpace(user.Username)

	token, err := l.loginCore.Login(user.Username, user.Password)
	if err != nil {
		logger.Error("failed login, err: ", err.Error())
		ctx.JSON(http.StatusNotFound, gin.H{"error": err.Error()})
		return
	}

	ctx.JSON(http.StatusOK, gin.H{"token": token})

}
