package handlers

import "github.com/gin-gonic/gin"

type Handler interface {
	InitializeRoute(r *gin.Engine)
}

// RegisterHandler ...
func RegisterHandler(r *gin.Engine, handlers ...Handler) {
	for _, h := range handlers {
		h.InitializeRoute(r)
	}
}
